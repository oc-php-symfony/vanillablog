<?php

namespace App\Domain\Post\Controller;

use App\Core\Application;
use App\Domain\Admin\Email\ContactEmail;
use App\Domain\Admin\Type\ContactType;
use App\Domain\Admin\Type\ContactValidator;
use App\Domain\Comment\Manager\CommentManager;
use App\Domain\Comment\Type\CommentType;
use App\Domain\Comment\Type\CommentValidator;
use App\Domain\Post\Entity\Post;
use App\Domain\Post\PostManager;
use App\Domain\Post\Type\PostType;
use App\Domain\Post\Type\PostValidator;
use App\Services\Controller\AbstractController;
use App\Services\Mailer\Mailjet\MJMailer;
use Exception;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Error\SyntaxError;

class PostController extends AbstractController {

    /**
     * @return string
     * @throws LoaderError
     * @throws RuntimeError
     * @throws SyntaxError
     * @throws Exception
     */
    public function home()
    {
        //On form submit
        if (Application::$request->post()) {

            $validator = new ContactValidator();

            if ($validator->isValid()) {

                $mailer = new MJMailer();

                $mail = new ContactEmail();

                $mailer->sendEmail($mail);

                Application::$request->addAlert([
                    'type' => 'success',
                    'title' => '<i class="fas fa-check"></i> Demande de contact',
                    'content' => 'Votre message à été envoyé'
                ]);

            } else {
                Application::$request->addAlert([
                    'type' => 'danger',
                    'title' => '<i class="fas fa-times"></i> Demande de contact',
                    'content' => 'Erreur dans le message'
                ]);
            }

            $this->redirect("/");
        }

        $contactForm = new ContactType('/');

        $postManager = new PostManager();

        $posts = $postManager->findAllActiveWithLimit(3);

        return $this->render('blog/home.html.twig', [
            'posts' => $posts,
            'contactForm' => $contactForm->generateForm()
        ]);
    }

    /**
     * @return string
     * @throws LoaderError
     * @throws RuntimeError
     * @throws SyntaxError
     */
    public function legals()
    {
        return $this->render('blog/legals.html.twig');
    }

    /**
     * @return string
     * @throws LoaderError
     * @throws RuntimeError
     * @throws SyntaxError
     */
    public function index()
    {
        $postManager = new PostManager();

        $posts = $postManager->findAllActive();

        return $this->render('blog/index.html.twig', [
            'posts' => $posts
        ]);
    }

    /**
     * @param $id
     * @return string
     * @throws LoaderError
     * @throws RuntimeError
     * @throws SyntaxError
     * @throws Exception
     */
    public function show($id)
    {
        //On form submit
        if (!empty(Application::$request->post())) {

            $validator = new CommentValidator();

            if (Application::$request->post('content') && $validator->isValid()) {

                $commentManager = new CommentManager();
                $commentManager->createComment($id, Application::$request->post('content'));

                Application::$request->addAlert([
                    'type' => 'success',
                    'title' => '<i class="fas fa-check"></i> Commentaire',
                    'content' => 'Votre commentaire à été envoyé, il sera validé prochainement'
                ]);

            } else {
                Application::$request->addAlert([
                    'type' => 'danger',
                    'title' => '<i class="fas fa-times"></i> Commentaire',
                    'content' => 'Erreur dans le commentaire, merci de réessayer'
                ]);
            }

            $this->redirect("/post/$id");
        }

        $commentForm = new CommentType('/post/'.$id);

        /** @var PostManager $postManager */
        $postManager = new PostManager();
        $post = $postManager->findFirstFromId($id);

        //If no post is found in DB
        if ($post->getId() === null) {
            Application::$request->addAlert([
                'type' => 'danger',
                'title' => '<i class="fas fa-times"></i> Article non trouvé',
                'content' => 'Oups, cet article n\'existe pas (plus ?)'
            ]);
            $this->redirect('/404');
        }
        $comments = $postManager->fetchPostValidatedComments($id);

        return $this->render('blog/show.html.twig', [
            'post' => $post,
            'comments' => $comments,
            'commentForm' => $commentForm->generateForm()
        ]);
    }

    /**
     * @return string
     * @throws LoaderError
     * @throws RuntimeError
     * @throws SyntaxError
     * @throws Exception
     */
    public function create()
    {
        //On form submit
        if (!empty(Application::$request->post())) {

            $validator = new PostValidator();

            if ($validator->isValid()) {
                $postManager = new PostManager();
                $post = new Post();
                $post->hydrate(Application::$request->post());

                $postManager->create($post);

                Application::$request->addAlert([
                    'type' => 'success',
                    'title' => '<i class="fas fa-check"></i> Création',
                    'content' => 'L\'article à bien été créé'
                ]);

            } else {
                Application::$request->addAlert([
                    'type' => 'danger',
                    'title' => '<i class="fas fa-times"></i> Création',
                    'content' => 'Erreur lors de la création de l\'article'
                ]);
            }

            $this->redirect("/admin/post");
        }

        $form = new PostType('/admin/post/new');

        return $this->render('blog/edit.html.twig', [
            'form' => $form->generateForm()
        ]);
    }

    /**
     * @param $id
     * @return string
     * @throws LoaderError
     * @throws RuntimeError
     * @throws SyntaxError
     * @throws Exception
     */
    public function edit($id)
    {
        /** @var PostManager $postManager */
        $postManager = new PostManager();
        $post = $postManager->findFirstFromId($id);

        //On form submit
        if (!empty(Application::$request->post())) {

            $validator = new PostValidator();

            if ($validator->isValid()) {
                $postManager->updateObject($id);

                Application::$request->addAlert([
                    'type' => 'primary',
                    'title' => '<i class="fas fa-check"></i> Mise à jour',
                    'content' => 'L\'article à bien été mis à jour'
                ]);

                $this->redirect("/admin/post");
            }


        }


        $form = new PostType('/admin/post/edit/'.$id, $post);

        return $this->render('blog/edit.html.twig', [
            'post' => $post,
            'form' => $form->generateForm()
        ]);
    }

    /**
     * @throws Exception
     */
    public function delete()
    {
        //Fetch of post comment for deletion
        $postId = Application::$request->post('id') ?: null;

        $commentManager = new CommentManager();
        $comments = $commentManager->fetchPostComments($postId);

        if (!empty($comments)) {
            $commentManager = new CommentManager();
            $commentManager->deleteAll($comments);
        }

        //Post deletion
        $postManager = new PostManager();
        $post = $postManager->findFirstFromId($postId);

        $postManager = new PostManager();
        $postManager->delete($post);

        Application::$request->addAlert([
            'type' => 'success',
            'title' => '<i class="fas fa-check"></i> Suppression',
            'content' => 'L\'article à bien été supprimé'
        ]);

        $this->redirect("/admin/post");
    }

    /**
 * @throws Exception
 */
    public function publish()
    {
        $postManager = new PostManager();
        $post = $postManager->findFirstFromId(Application::$request->post('id'));
        $postManager->publish($post);

        Application::$request->addAlert([
            'type' => 'success',
            'title' => '<i class="fas fa-check"></i> Publication',
            'content' => 'L\'article à bien été publié'
        ]);

        $this->redirect("/admin/post");
    }

    /**
     * @throws Exception
     */
    public function unpublish()
    {
        $postManager = new PostManager();
        $post = $postManager->findFirstFromId(Application::$request->post('id'));
        $postManager->unpublish($post);

        Application::$request->addAlert([
            'type' => 'success',
            'title' => '<i class="fas fa-check"></i> Mise en brouillon',
            'content' => 'L\'article à bien été mis en brouillon'
        ]);

        $this->redirect("/admin/post");
    }

}

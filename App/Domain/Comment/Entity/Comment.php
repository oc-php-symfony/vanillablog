<?php

namespace App\Domain\Comment\Entity;

use App\Services\Entity\AbstractEntity;

class Comment extends AbstractEntity
{
    public static $noPersist = [
        'username',
        'updated_at',
        'created_at',
        'commentStatus',
        'status_name'
    ];

    private $id;
    private $content;
    private $createdAt;
    private $updatedAt;
    private $username;
    private $userId;
    private $postId;
    private $commentStatusId;
    private $statusName;
    private $unregisteredUsername;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * @param mixed $content
     */
    public function setContent($content)
    {
        $this->content = $content;
    }

    /**
     * @return mixed
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @param mixed $createdAt
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;
    }

    /**
     * @return mixed
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * @param mixed $updatedAt
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;
    }

    /**
     * @return mixed
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * @param mixed $username
     */
    public function setUsername($username)
    {
        $this->username = $username;
    }

    /**
     * @return mixed
     */
    public function getUserId()
    {
        return $this->userId;
    }

    /**
     * @param mixed $userId
     */
    public function setUserId($userId): void
    {
        $this->userId = $userId;
    }

    /**
     * @return mixed
     */
    public function getPostId()
    {
        return $this->postId;
    }

    /**
     * @param mixed $postId
     */
    public function setPostId($postId)
    {
        $this->postId = $postId;
    }

    /**
     * @return mixed
     */
    public function getCommentStatusId()
    {
        return $this->commentStatusId;
    }

    /**
     * @param mixed $commentStatusId
     */
    public function setCommentStatusId($commentStatusId)
    {
        $this->commentStatusId = $commentStatusId;
    }

    /**
     * @return mixed
     */
    public function getStatusName()
    {
        return $this->statusName;
    }

    /**
     * @param mixed $statusName
     */
    public function setStatusName($statusName)
    {
        $this->statusName = $statusName;
    }

    /**
     * @return mixed
     */
    public function getUnregisteredUsername()
    {
        return $this->unregisteredUsername;
    }

    /**
     * @param mixed $unregisteredUsername
     */
    public function setUnregisteredUsername($unregisteredUsername)
    {
        $this->unregisteredUsername = $unregisteredUsername;
    }


}
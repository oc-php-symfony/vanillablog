<?php

namespace App\Domain\Comment\Controller;

use App\Core\Application;
use App\Domain\Comment\Manager\CommentManager;
use App\Services\Controller\AbstractController;
use Exception;

class CommentController extends AbstractController
{

    /**
     * @throws Exception
     */
    public function delete()
    {
        //Fetch of post comment for deletion
        $commentManager = new CommentManager();

        //Comment deletion
        if (Application::$request->post('id')) {
            $comment = $commentManager->findFirstFromId(Application::$request->post('id'));
        } else $comment = null;

        $commentManager->delete($comment);

        Application::$request->addAlert([
            'type' => 'success',
            'title' => '<i class="fas fa-check"></i> Suppression',
            'content' => 'Le commentaire à bien été supprimé'
        ]);

        $this->redirect("/admin/comment");
    }

    /**
     * @throws Exception
     */
    public function moderate()
    {
        $commentManager = new CommentManager();

        if (Application::$request->post('id')) {
            $comment = $commentManager->findFirstFromId(Application::$request->post('id'));
        } else $comment = null;

        $commentManager->moderate($comment);

        Application::$request->addAlert([
            'type' => 'success',
            'title' => '<i class="fas fa-check"></i> Modération',
            'content' => 'Le commentaire à été modéré'
        ]);

        $this->redirect("/admin/comment");
    }

    /**
     * @throws Exception
     */
    public function validate()
    {
        $commentManager = new CommentManager();

        if (Application::$request->post('id')) {
            $comment = $commentManager->findFirstFromId(Application::$request->post('id'));
        } else $comment = null;

        $commentManager->validate($comment);

        Application::$request->addAlert([
            'type' => 'success',
            'title' => '<i class="fas fa-check"></i> Validation',
            'content' => 'Le commentaire à été validé'
        ]);

        $this->redirect("/admin/comment");
    }
}
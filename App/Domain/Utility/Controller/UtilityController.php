<?php

namespace App\Domain\Utility\Controller;

use App\Services\Controller\AbstractController;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Error\SyntaxError;

class UtilityController extends AbstractController
{
    /**
     * @return string
     * @throws LoaderError
     * @throws RuntimeError
     * @throws SyntaxError
     */
    public function notFound()
    {
        return $this->render('utility/404.html.twig');
    }

    /**
     * @return string
     * @throws LoaderError
     * @throws RuntimeError
     * @throws SyntaxError
     */
    public function forbidden()
    {
        return $this->render('utility/403.html.twig');
    }
}
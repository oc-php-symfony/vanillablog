<?php

namespace App\Domain\Admin\Controller;

use App\Domain\Comment\Entity\Comment;
use App\Domain\Comment\Entity\CommentStatus;
use App\Domain\Comment\Manager\CommentManager;
use App\Domain\Comment\Repository\CommentRepository;
use App\Domain\Post\Entity\Post;
use App\Domain\Post\Entity\PostStatus;
use App\Domain\Post\PostManager;
use App\Domain\Post\Repository\PostRepository;
use App\Domain\User\Entity\User;
use App\Domain\User\Manager\UserManager;
use App\Services\Controller\AbstractController;
use Exception;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Error\SyntaxError;

class AdminController extends AbstractController
{
    /**
     * Homepage for admin
     *
     * @return string
     * @throws LoaderError
     * @throws RuntimeError
     * @throws SyntaxError
     */
    public function home()
    {
        //Aggregation a voir
        return $this->render('admin/home.html.twig');
    }

    /**
     * Post management for admins
     *
     * @return string
     * @throws LoaderError
     * @throws RuntimeError
     * @throws SyntaxError
     */
    public function post()
    {
        $postRepository = new PostRepository();

        $posts = $postRepository->findAll(Post::class,
            ['fields' => 'post.*, user.username, post_status.status_name'],
            [PostStatus::class => 'post_status_id', User::class => 'author_id']);

        return $this->render('admin/post.html.twig', [
            'posts' => $posts
        ]);
    }

    /**
     * Comment management for admins
     *
     * @param null $postId
     * @return string
     * @throws LoaderError
     * @throws RuntimeError
     * @throws SyntaxError
     * @throws Exception
     */
    public function comment($postId = null)
    {
        $postManager = new PostManager();
        $commentManager = new CommentManager();

        if ($postId) {
            $comments = $commentManager->fetchPostComments($postId);

            $post = $postManager->findFirstFromId($postId);

            return $this->render('admin/comment.html.twig', [
                'comments' => $comments,
                'post' => $post
            ]);

        } else {
            $comments = $commentManager->findAll(Comment::class,
                ['fields' => 'comment.*, user.username, comment_status.status_name'],
                [User::class => 'user_id', CommentStatus::class => 'comment_status_id']);

            return $this->render('admin/comment.html.twig', [
                'comments' => $comments
            ]);
        }
    }

    /**
     * @return string
     * @throws LoaderError
     * @throws RuntimeError
     * @throws SyntaxError
     */
    public function user()
    {
        $userManager = new UserManager();

        $users = $userManager->findAll();

        return $this->render('admin/user.html.twig', [
            'users' => $users
        ]);
    }
}
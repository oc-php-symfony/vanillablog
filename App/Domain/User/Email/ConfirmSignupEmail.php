<?php

namespace App\Domain\User\Email;

use App\Core\Application;
use App\Domain\User\Entity\User;
use App\Services\Mailer\Mailjet\MJAbstractEmail;
use App\Services\Mailer\Mailjet\MJContact;
use App\Services\Mailer\Mailjet\MJVariable;

class ConfirmSignupEmail extends MJAbstractEmail
{
    public function __construct(User $user)
    {
        //Mailjet template ID
        $this->templateId = 2030328;

        $this->subject = 'Confirmation de votre inscription';

        $this->to = new MJContact();
        $this->to->name = $user->getUsername();
        $this->to->email = $user->getEmail();

        $this->from = new MJContact();
        $this->from->name = 'I/O Blog housekeeping';
        $this->from->email = 'damien.adam@heureetcontrole.fr';

        if (Application::$request->server('HTTP_HOST')) {
            $confirmUrl = new MJVariable();
            $confirmUrl->key = 'confirmUrl';
            $confirmUrl->value = Application::$request->server('HTTP_HOST').'/signin-confirm/'.$user->getConfirmSignupToken();
            $this->vars[] = $confirmUrl;
        }

        $name = new MJVariable();
        $name->key = 'name';
        $name->value = $user->getUsername();
        $this->vars[] = $name;
    }

}
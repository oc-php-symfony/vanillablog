<?php

namespace App\Domain\User\Type;

use App\Services\Validator\AbstractValidator;

class SigninValidator extends AbstractValidator
{
    /**
     * SigninValidator constructor.
     *
     * password maxsize is 72 for bcrypt hash limit
     */
    public function __construct()
    {
        $this->constraints = [
            'username' => [
                'notNull' => true,
                'minSize' => 4,
                'maxSize' => 200
            ],
            'email' => [
                'notNull' => true,
                'minSize' => 6,
                'maxSize' => 2000,
                'emailValidation' => true
            ],
            'password' => [
                'notNull' => true,
                'confirm' => 'confirm_password',
                'minSize' => 6,
                'maxSize' => 72
            ]
        ];
    }
}
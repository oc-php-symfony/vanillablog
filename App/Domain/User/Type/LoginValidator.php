<?php

namespace App\Domain\User\Type;

use App\Services\Validator\AbstractValidator;

class LoginValidator extends AbstractValidator
{
    /**
     * LoginValidator constructor.
     */
    public function __construct()
    {
        $this->constraints = [
            'username' => [
                'notNull' => true
            ],
            'password' => [
                'notNull' => true
            ]
        ];
    }
}
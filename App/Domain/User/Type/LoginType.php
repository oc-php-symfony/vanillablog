<?php

namespace App\Domain\User\Type;

use App\Services\Type\AbstractType;
use App\Services\Type\PasswordType;
use App\Services\Type\SubmitType;
use App\Services\Type\TextType;

/**
 * Class LoginType
 *
 * Form used to login
 *
 * @package App\Domain\User\Type
 */
class LoginType extends AbstractType
{
    public function generateForm()
    {
        $this
            ->add('username', TextType::class, [
                'label' => 'Nom d\'utilisateur',
                'class' => ''
            ])
            ->add('password', PasswordType::class, [
                'label' => 'Mot de passe',
                'class' => ''
            ])
            ->add('submit', SubmitType::class, [
                'label' => 'Se connecter',
                'class' => 'btn btn-primary',
            ])
        ;

        return $this->render();
    }
}
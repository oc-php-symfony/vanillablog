<?php

namespace App\Domain\User\Manager;

use App\Core\Application;
use App\Domain\User\Email\ConfirmSignupEmail;
use App\Domain\User\Entity\User;
use App\Domain\User\Entity\UserRole;
use App\Domain\User\Entity\UserStatus;
use App\Domain\User\Repository\RoleRepository;
use App\Domain\User\Repository\StatusRepository;
use App\Domain\User\Repository\UserRepository;
use App\Services\Mailer\Mailjet\MJMailer;
use App\Services\Utilities\StringUtilities;
use Exception;

/**
 * Class UserManager
 *
 * Utilities for user manipulation and management
 *
 * @package App\Domain\User\Manager
 */
class UserManager
{

    /**
     * @var UserRepository
     */
    private $userRepository;

    public function __construct()
    {
        $this->userRepository = new UserRepository();
    }

    /**
     * @param User $user
     * @throws Exception
     */
    public function validateUser(User $user)
    {
        $this->userRepository->updateObject(
            User::class,
            $user->getId(),
            ['user_status_id'],
            [$this->getStatusId('valid')]
        );
    }

    /***
     * Check if username already exists in database
     * @param $username
     * @return bool
     * @throws Exception
     */
    public function usernameExists($username)
    {
        /** @var User $user */
        $user = $this->userRepository->findFirst(User::class, ['where' => [
            ['field' => 'username',
            'operator' => '=',
            'value' => $username]
        ]]);

        /** findFirst return obligatoirement un objet met les attributs sont NULL si le résultat PDO est vide */
        return !empty($user->getUsername());
    }

    /**
     * Checks if email already exists in database
     * @param $email
     * @return bool
     * @throws Exception
     */
    public function emailExists($email)
    {
        /** @var User $user */
        $user = $this->userRepository->findFirst(User::class, ['where' => [
            ['field' => 'email',
            'operator' => '=',
            'value' => $email]
        ]]);

        /** findFirst return obligatoirement un objet mais les attributs sont NULL si le résultat PDO est vide */
        return !empty($user->getEmail());
    }

    /**
     * @param $username
     * @param $email
     * @param $password
     * @return User
     * @throws Exception
     */
    public function createUser($username, $email, $password)
    {
        $user = new User();
        $salt = StringUtilities::generateRandomString(40);
        $user = ($user)
            ->setUsername($username)
            ->setEmail($email)
            ->setConfirmSignupToken(StringUtilities::generateRandomString(35))
            ->setPasswordSalt($salt)
            ->setPasswordHash($this->hashPassword($password, $salt))
            ->setUserStatusId($this->getStatusId('pending'))
            ->setUserRoleId($this->getRoleId('member'))
        ;

        $this->userRepository->create($user);

        return $user;
    }

    /**
     * @param $username
     * @param $password
     * @return bool
     * @throws Exception
     */
    public function login($username, $password)
    {
        $pepper = Application::$request->env('PEPPER');
        $userSalt = $this->retrieveSalt($username);
        $pwdSaltAndPeppered = hash_hmac("sha256", $password, $userSalt.$pepper);
        $pwdHashed = $this->retrievePasswordHash($username);

        if (password_verify($pwdSaltAndPeppered, $pwdHashed)) {

            $user = $this->userRepository->findFirst(User::class,
                ['where' => [
                    ['field' => 'username',
                    'operator' => '=',
                    'value' => $username]
                 ], 'fields' => 'user.*, user_role.role_name, user_status.status_name'
                ], [UserStatus::class => 'user_status_id', UserRole::class => 'user_role_id']);

            Application::$request->setSession('user', $user);

            return true;
        }

        return false;
    }

    public function logout()
    {
        Application::$request->setSession('user', null);
        unset($_SESSION['user']);
    }

    /**
     * @param $statusName
     * @return mixed
     * @throws Exception
     */
    public function getStatusId($statusName)
    {
        $statusRepository = new StatusRepository();

        $status = $statusRepository->findFirst(UserStatus::class, ['where' => [
            ['field' => 'status_name',
            'operator' => '=',
            'value' => $statusName]
        ]]);

        return $status->getId();
    }

    /**
     * @param $roleName
     * @return mixed
     * @throws Exception
     */
    public function getRoleId($roleName)
    {
        $roleRepository = new RoleRepository();

        $role = $roleRepository->findFirst(UserRole::class, ['where' => [
            ['field' => 'role_name',
            'operator' => '=',
            'value' => $roleName]
        ]]);

        return $role->getId();
    }

    /**
     * @param $username
     * @return mixed
     * @throws Exception
     */
    private function retrievePasswordHash($username)
    {
        /** @var User $user */
        $user = $this->userRepository->findFirst(User::class, ['where' => [
            ['field' => 'username',
            'operator' => '=',
            'value' => $username]
        ]]);

        return $user->getPasswordHash();
    }

    /**
     * @param $password
     * @return false|string|null
     */
    private function hashPassword($password, $salt)
    {
        $pepper = Application::$request->env('PEPPER');
        $pwdSaltAndPeppered = hash_hmac("sha256", $password, $salt.$pepper);
        return password_hash($pwdSaltAndPeppered, PASSWORD_BCRYPT);
    }

    public function findAll()
    {
        return $this->userRepository->findAll(User::class,
            ['fields' => 'user.*, user_role.role_name, user_status.status_name'],
            [UserStatus::class => 'user_status_id', UserRole::class => 'user_role_id']);
    }

    public function sendConfirmSignupEmail($user)
    {
        $mailer = new MJMailer();
        $mail = new ConfirmSignupEmail($user);
        return $mailer->sendEmail($mail);
    }

    /**
     * @param string $token
     * @return User
     * @throws Exception
     */
    public function findFromConfirmSigninToken(string $token)
    {
        /** @var User $user */
        $user = $this->userRepository->findFirst(User::class, ['where' => [
            ['field' => 'confirm_signup_token',
            'operator' => '=',
            'value' => $token]
        ]]);

        return $user;
    }

    /**
     * @param $username
     * @return mixed
     * @throws Exception
     */
    private function retrieveSalt($username)
    {
        /** @var User $user */
        $user = $this->userRepository->findFirst(User::class, ['where' => [
            ['field' => 'username',
                'operator' => '=',
                'value' => $username]
        ]]);

        return $user->getPasswordSalt();
    }
}
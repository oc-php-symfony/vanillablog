<?php

namespace App\Core;

class Request
{
    private $_post;
    private $_get;
    private $_server;
    private $_env;

    public function __construct()
    {
        $this->_post = $_POST;
        array_map([$this, 'escapePost'], array_keys($this->_post), $this->_post);
        $this->_get = $_GET;
        $this->_server = $_SERVER;
        $this->_env = $_ENV;
    }

    public function post($key = null, $default = null)
    {
        return $this->checkGlobal($this->_post, $key, $default);
    }

    public function get($key = null, $default = null)
    {
        return $this->checkGlobal($this->_get, $key, $default);
    }

    public function session($key = null, $default = null)
    {
        return $this->checkGlobal($_SESSION, $key, $default);
    }

    public function setSession($key, $value)
    {
        $_SESSION[$key] = $value;
    }

    public function addAlert(array $alert)
    {
        $_SESSION['alerts'][] = $alert;
    }

    public function server($key = null, $default = null)
    {
        return $this->checkGlobal($this->_server, $key, $default);
    }

    public function env($key = null, $default = null)
    {
        return $this->checkGlobal($this->_env, $key, $default);
    }

    private function checkGlobal($global, $key = null, $default = null)
    {
        if ($key) {
            if (isset($global[$key])) {
                return $global[$key];
            } else {
                return $default ?: null;
            }
        }
        return $global;
    }

    private function escapePost($key, $value)
    {
        //Wisywig has html chars but only in admin so it's ok
        if ($key !== 'content') {
            return htmlspecialchars($value);
        }

        return $value;

    }

}
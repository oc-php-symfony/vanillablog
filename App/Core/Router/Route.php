<?php

namespace App\Core\Router;

/**
 * Class Route represents a single url route in the application
 *
 * @package App\Core\Router
 */
class Route
{

    private $path;
    private $callable;
    private $methodName;
    private $guardLevel;
    private $matches = [];

    public function __construct($path, $callable, $methodName, $guardLevel = null)
    {
        $this->path = trim($path, '/');
        $this->callable = $callable;
        $this->methodName = $methodName;
        $this->guardLevel = $guardLevel;
    }

    /* Permettra de capturer l'url avec les paramètre * get('/posts/:slug-:id') par exemple */
    public function match($url)
    {
        $url = trim($url, '/');
        $path = preg_replace('#:([\w]+)#', '([^/]+)', $this->path);
        $regex = "#^$path$#i";
        if (!preg_match($regex, $url, $matches)) {
            return false;
        }
        array_shift($matches);
        $this->matches = $matches;  // On sauvegarde les paramètre dans l'instance pour plus tard
        return true;
    }

    public function call(){

        $controller = new $this->callable();

        return $controller->{$this->methodName}(...$this->matches);
    }

    /**
     * @return null
     */
    public function getGuardLevel()
    {
        return $this->guardLevel;
    }

}
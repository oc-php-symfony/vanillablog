<?php

namespace App\Core\Router;

use App\Core\Application;
use App\Core\Router\RouterException;

/**
 * Class Router
 * Router is instantiated at application entry point
 *
 * Hold routes declaration
 * Routes current url request to associated controller and method
 *
 * @package App\Core\Router
 */
class Router {

    private $url;
    private $routes = [];
    private $namedRoutes = [];

    public function __construct($url){
        $this->url = $url;
    }

    public function registerRoutes()
    {
        $jsonRoutes = file_get_contents(Application::$ROOT_PATH."/App/Core/Router/routes.json");
        $routes = json_decode($jsonRoutes, true);

        foreach ($routes as $route) {

            $processor = explode('::', $route['processor']);
            $controller = $processor[0];
            $function = $processor[1];
            $method = $route['method'];
            $guardLevel = null;

            if (array_key_exists("guard", $route)) {
                $guardLevel = $route['guard'];
            }

            $this->$method($route['url'], $controller, $function, $guardLevel);
        }
    }

    /**
     * Adds a route with a GET method
     *
     * @param $path
     * @param $callable
     * @param $methodName
     * @param $guardLevel
     * @param null $name
     * @return Route
     */
    public function get($path, $callable, $methodName, $guardLevel, $name = null)
    {

        return $this->add($path, $callable, $methodName, $name, $guardLevel, 'GET');
    }

    /**
     * Adds a route with a POST method
     *
     * @param $path
     * @param $callable
     * @param $methodName
     * @param $guardLevel
     * @param null $name
     * @return Route
     */
    public function post($path, $callable, $methodName, $guardLevel, $name = null)
    {

        return $this->add($path, $callable, $methodName, $name, $guardLevel, 'POST');
    }

    /**
     * Adds the route to the router routes
     *
     * @param $path
     * @param $callable
     * @param $methodName
     * @param $name
     * @param $guardLevel
     * @param $method
     * @return Route
     */
    private function add($path, $callable, $methodName, $name, $guardLevel, $method)
    {
        $route = new Route($path, $callable, $methodName, $guardLevel);
        $this->routes[$method][] = $route;

        if(is_string($callable) && $name === null){
            $name = $callable;
        }

        if($name){
            $this->namedRoutes[$name] = $route;
        }

        return $route;
    }

    /**
     * Compare known routes to url and calls associated controller method
     *
     * @throws RouterException
     */
    public function run(){

        if(!isset($this->routes[Application::$request->server('REQUEST_METHOD')]))
        {
            throw new RouterException('this REQUEST_METHOD does not exist');
        }

        /** @var Route $route */
        foreach($this->routes[Application::$request->server('REQUEST_METHOD')] as $route){
            if($route->match($this->url)){

                if ($this->hasAccess($route->getGuardLevel())) {
                    echo $route->call();

                    return;
                }

                $forbidden = new Route('/403', "App\\Domain\\Utility\\Controller\\UtilityController", "forbidden");
                echo $forbidden->call();

                return;
            }

        }

        $notFound = new Route('/404', "App\\Domain\\Utility\\Controller\\UtilityController", "notFound");
        echo $notFound->call();

        return;
    }

    /**
     * Returns url from $_GET['url']
     * Router is instantiated at application entry point
     *
     *
     * @param $name
     * @param array $params
     * @return mixed
     * @throws RouterException
     */
    public function url($name, $params = [])
    {
        if(!isset($this->namedRoutes[$name])){
            throw new RouterException('No route matches this name');
        }

        return $this->namedRoutes[$name]->getUrl($params);
    }

    private function hasAccess($guardLevel)
    {
        if ($guardLevel !== null) {
            $authorizedLevels = explode(', ', $guardLevel);

            if (array_key_exists("user", Application::$request->session())
                && in_array(Application::$request->session('user')->getRoleName(), $authorizedLevels)) {
                return true;
            }

            return false;
        }

        return true;
    }

}
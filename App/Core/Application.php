<?php

namespace App\Core;

use App\Core\Router\Router;
use App\Core\Router\RouterException;

class Application
{
    public static $ROOT_PATH;
    public $router;
    public static $request;

    /**
     * Application constructor.
     * @param $rootPath
     * @throws RouterException
     */
    public function __construct($rootPath)
    {
        self::$ROOT_PATH = $rootPath;
        session_start();

        self::$request = new Request();

        $this->handleSession();
        if (self::$request->get('url') !== null) {
            $this->router = new Router(self::$request->get('url') ?: '/');
        }

        $this->router->registerRoutes();
        $this->router->run();
    }

    private function handleSession()
    {
        if (self::$request->session('redirect') && self::$request->session('redirect') === true) {
            self::$request->setSession('redirect', false);
        } else {
            self::$request->setSession('alerts', []);
        }
    }
}
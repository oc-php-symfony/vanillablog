<?php

namespace App\Services\Type;

use App\Core\Application;
use App\Services\Utilities\StringUtilities;

/**
 * Holds and returns common form building tags
 *
 * Class AbstractType
 * @package App\Services\Type
 */
class AbstractType
{
    private $formStart;
    private $formEnd;
    private $fields;
    private $editObject;

    /**
     * AbstractType constructor.
     *
     * Will build basic skeleton for html form
     *
     * @param $submitUrl
     * @param null $editObject
     * @param string $formMethod
     */
    public function __construct($submitUrl, $editObject = null, $formMethod = 'POST')
    {
        $this->formStart = "<form method='$formMethod' action='$submitUrl'>";
        $this->formEnd = "</form>";
        $this->fields = [];
        $this->editObject = $editObject;
    }

    /**
     * Adds form input to the main form being built
     *
     * @param $fieldName
     * @param $fieldType
     * @param array $options
     * @return $this
     */
    public function add($fieldName, $fieldType, array $options)
    {
        $element = new $fieldType($fieldName, $options, $this->editObject);

        $this->fields[] = "<div class='form-group'>" . $element->getHtmlElement() . "</div>";

        return $this;
    }

    /**
     * Renders full built form (call last)
     *
     * @return string
     */
    public function render(): string
    {
        $innerForm = $this->getInnerFormFromAttrArray();

        return $this->formStart . $innerForm . $this->formEnd;
    }

    /**
     * Concatenate all form input to get the inner html form
     *
     * @return string
     */
    private function getInnerFormFromAttrArray(): string
    {
        $innerForm = "";

        //Create a random token and put it in the session and in the form
        $csrfToken = md5(uniqid());
        Application::$request->setSession('csrfToken', $csrfToken);

        $innerForm .= "<input name='csrfToken' type='hidden' value='$csrfToken'>";

        foreach ($this->fields as $field) {
            $innerForm .= $field;
        }

        return $innerForm;
    }
}
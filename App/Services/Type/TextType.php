<?php

namespace App\Services\Type;

/**
 * Class TextType
 *
 * Build a text input html form element
 *
 * @package App\Services\Type
 */
class TextType extends AbstractFormElement
{
    public function __construct($fieldName, array $options, $editObject = null)
    {

        $value = null;

        if ($editObject) {
            $method = 'get' . ucfirst($fieldName);
            $value = $editObject->$method() ?: null;
        }

        $this->htmlElement = "<label for='{$fieldName}'>{$options['label']}</label>";

        $this->htmlElement .= "<input name='{$fieldName}' type='text' class='form-control {$options['class']}' id='{$fieldName}' value='{$value}'>";
    }
}
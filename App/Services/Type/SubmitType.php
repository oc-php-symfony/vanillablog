<?php

namespace App\Services\Type;

/**
 * Class SubmitType
 *
 * Build a submit html form element
 *
 * @package App\Services\Type
 */
class SubmitType extends AbstractFormElement
{
    public function __construct($fieldName, array $options)
    {
        $this->htmlElement = "<button type='submit' class='{$options['class']}'>{$options['label']}</button>";
    }
}
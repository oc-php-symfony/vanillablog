<?php

namespace App\Services\Type;

class AbstractFormElement
{
    protected $htmlElement;

    /**
     * @return mixed
     */
    public function getHtmlElement()
    {
        return $this->htmlElement;
    }

}
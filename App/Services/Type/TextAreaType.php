<?php


namespace App\Services\Type;

/**
 * Class TextAreaType
 *
 * Build a text area html form element
 *
 * @package App\Services\Type
 */
class TextAreaType extends AbstractFormElement
{
    public function __construct($fieldName, array $options, $editObject = null)
    {
        $value = null;

        if ($editObject) {
            $method = 'get' . ucfirst($fieldName);
            $value = $editObject->$method() ?: null;
        }

        $html = "<label for='{$fieldName}'>{$options['label']}</label>";

        if (isset($options['wisywig']) && $options['wisywig'] === true) {
            $html .= "<textarea name='{$fieldName}' class='form-control {$options['class']}' id='editor'>$value</textarea>";
        } else {
            $html .= "<textarea name='{$fieldName}' class='form-control {$options['class']}' id='{$fieldName}'></textarea>";
        }

        $this->htmlElement = $html;
    }

}
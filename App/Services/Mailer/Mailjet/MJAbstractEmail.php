<?php

namespace App\Services\Mailer\Mailjet;

class MJAbstractEmail
{
    public $templateId;

    public $subject;

    /** @var MJContact $from */
    public $from;

    /** @var MJContact $to */
    public $to;

    /**
     * Array of 0-N MJVariable
     * @var array
     */
    public $vars = [];

    /**
     * Array o 0-N MailjetAttachment
     * @var array
     */
    public $attachments = [];

    public function getBody()
    {
        $body = [
            'Messages' => [
                [
                    'From' => [
                        'Email' => $this->from->email,
                        'Name' => $this->from->name,
                    ],
                    'To' => [
                        [
                            'Email' => $this->to->email,
                            'Name' => $this->to->name
                        ]
                    ],
                    'Subject' => $this->subject,
                    'TemplateID' => $this->templateId,
                    'TemplateLanguage' => true
                ]
            ]
        ];

        if (!empty($this->vars)) {

            $variables = '{';

            /** @var MJVariable $variable */
            foreach ($this->vars as $variable) {
                $variables .= '"'.$variable->key.'": "'.$variable->value.'", ';
            }

            $variables = rtrim($variables, ', ');
            $variables .= '}';

            $body['Messages'][0]['Variables'] = json_decode($variables, true);

        }

        if (!empty($this->attachments)) {
            /** @var MJAttachment $attachment */
            foreach ($this->attachments as $attachment) {
                $bodyAttachment = [];
                $bodyAttachment['Content-type'] = $attachment->contentType;
                $bodyAttachment['Filename'] = $attachment->fileName;
                $bodyAttachment['Base64Content'] = $attachment->content;

                $body['Messages'][0]['Attachments'][] = $bodyAttachment;
            }
        }

        return $body;
    }
}
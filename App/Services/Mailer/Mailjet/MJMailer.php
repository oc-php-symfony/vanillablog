<?php

namespace App\Services\Mailer\Mailjet;

use App\Core\Application;
use Mailjet\Client;
use Mailjet\Resources;

class MJMailer
{
    private $mj;

    public function __construct()
    {
        $this->mj = new Client(Application::$request->env('MJ_APIKEY_PUBLIC'),
            Application::$request->env('MJ_APIKEY_PRIVATE'),true,['version' => 'v3.1']);
    }

    public function sendEmail(MJAbstractEmail $mail)
    {
        $body = $mail->getBody();

        return $this->mj->post(Resources::$Email, [
            'body' => $body
        ]);
    }
}



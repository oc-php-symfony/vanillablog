<?php

namespace App\Services\Validator;

use App\Core\Application;
use Exception;

/**
 * Provides validation methods for form validation
 *
 * Class AbstractValidator
 * @package App\Services\Validator
 */
class AbstractValidator
{
    protected $constraints;

    /**
     * Return True if constraints are fulfilled
     *
     * @throws Exception
     */
    public function isValid()
    {
        $valid = true;

        //Host check for CORS breach
        if (Application::$request->server('HTTP_HOST') !== Application::$request->env('HOST')) {
            return false;
        }

        //CSRF check
        if (Application::$request->post('csrfToken') !== Application::$request->session('csrfToken')) {
            return false;
        }

        //Will check validity of all form attributes
        foreach ($this->constraints as $attribute => $constraint) {
            if (!Application::$request->post($attribute)) {
                throw new Exception('Missing form attribute in ' . get_class($this));
            }

            foreach ($constraint as $constraintName => $constraintValue) {
                $valid = $this->$constraintName(Application::$request->post($attribute), $constraintValue);

                if ($valid === false) {

                    return $valid;
                }
            }
        }

        return $valid;
    }

    /**
     * Check if value is null
     *
     * @param $value
     * @param $constraintValue
     * @return bool
     */
    protected function notNull($value, $constraintValue)
    {
        if ($constraintValue) {

            return $value === null ? false : true;

        } else return true;
    }

    /**
     * Check is value is bigger than constraint
     *
     * @param $value
     * @param $constraintValue
     * @return bool
     */
    protected function minSize($value, $constraintValue)
    {

        return strlen($value) >= $constraintValue;
    }

    /**
     * Check is value is smaller than constraint
     *
     * @param $value
     * @param $constraintValue
     * @return bool
     */
    protected function maxSize($value, $constraintValue)
    {

        return strlen($value) <= $constraintValue;
    }

    /**
     * Compares $value with its confirm equivalent
     *
     * @param $value
     * @param $constraintValue
     * @return bool
     */
    protected function confirm($value, $constraintValue)
    {
        if ($constraintValue) {

            return $value === Application::$request->post($constraintValue);
        } else return true;
    }

    /**
     * Return true if email is valid according to RFC 822
     * False otherwise
     *
     * @param $value
     * @param $constraintValue
     * @return bool|mixed
     */
    protected function emailValidation($value, $constraintValue)
    {
        return $constraintValue ? $this->emailValid($value) : true;
    }

    /**
     * @param $email
     * @return mixed
     */
    private function emailValid($email)
    {
        return filter_var($email, FILTER_VALIDATE_EMAIL);
    }
}
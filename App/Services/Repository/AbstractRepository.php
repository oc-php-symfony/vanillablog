<?php

namespace App\Services\Repository;

use App\Services\Database\MySQLConnector;
use App\Services\Database\MySQLQuery;
use App\Services\Database\MySQLQueryBuilder;
use App\Services\Utilities\StringUtilities;
use Exception;
use PDO;
use PDOException;

/**
 * Class AbstractRepository
 *
 * Base class for every entities repositories
 * Provides basic CRUD method see method comments for usage
 *
 * @package App\Services\Repository
 */
class AbstractRepository
{
    private $pdo;

    private $query;

    private $mySQLQueryBuilder;

    public function __construct()
    {
        $this->pdo = MySQLConnector::getInstance();
        $this->mySQLQueryBuilder = new MySQLQueryBuilder();
        $this->query = new MySQLQuery();
    }

    /**
     * Return a collection of objects of the same type
     *
     * @param $className
     * @param array $joins
     * @param array $options
     * @return array
     */
    public function findAll($className, array $options, array $joins = null)
    {
        $this->query->setTable($this->tableNameFromObject($className));

        //Avoid fields collision for joins i.e., post.id and status.id
        $this->query->setFields(isset($options['fields']) ? $options['fields'] : '*');

        if ($joins !== null) {
            $this->query->setJoins($joins);
        }

        //Where clause only effective on the request main table
        if (isset($options['where'])) {
            $this->query->setWhereClause($options['where']);
        }

        //Order clause
        if (isset($options['order'])) {
            $orderWay = $options['order']['way'];
            $orderAttribute = StringUtilities::toSnakeCase($options['order']['attribute']);
            $this->query->setOrderClause(" ORDER BY {$this->query->getTable()}.$orderAttribute $orderWay");
        }

        //Limit clause
        if (isset($options['limit'])) {
            $limitStart = $options['limit']['start'];
            $limitAmount = $options['limit']['amount'];
            $this->query->setLimitClause(" LIMIT $limitStart, $limitAmount");
        }

        $sth = $this->pdo->prepare($this->mySQLQueryBuilder->buildFindAll($this->query));

        $sth->execute(!empty($this->query->getPreparedValues())
            ? $this->query->getPreparedValues()
            : null
        );

        $arrayResults = $sth->fetchAll(PDO::FETCH_ASSOC);

        return $this->hydrateArray($arrayResults, $className);
    }

    /**
     * Returns a single objects, minimal criteria is 'class' and 'id' of said object
     *
     * @param $className
     * @param array $options
     * @param array|null $joins
     * @return mixed
     * @throws Exception
     */
    public function findFirst($className, array $options, array $joins = null)
    {
        $this->query->setTable($this->tableNameFromObject($className));

        //Avoid fields collision for joins i.e., post.id and status.id
        $this->query->setFields(isset($options['fields']) ? $options['fields'] : '*');

        if ($joins !== null) {
            $this->query->setJoins($joins);
        }

        //Where clause only effective on the request main table
        if (isset($options['where'])) {
            $this->query->setWhereClause($options['where']);
        } else {
            throw new Exception('No criteria provided for findFirstMethod');
        }

        $sth = $this->pdo->prepare($this->mySQLQueryBuilder->buildFindFirst($this->query));

        $sth->execute(!empty($this->query->getPreparedValues())
            ? $this->query->getPreparedValues()
            : null
        );

        $arrayResult = $sth->fetchAll(PDO::FETCH_ASSOC);

        $objectResult = new $className();

        if (!empty($arrayResult)) {
            $objectResult->hydrate($arrayResult[0]);
        }

        return $objectResult;
    }

    /**
     * Updates every fields provided of a DB entry
     *
     * @param $className
     * @param $objectId
     * @param array $columns
     * @param array $values
     */
    public function updateObject($className, $objectId, array $columns, array $values)
    {
        //Récupération de la table
        $this->query->setTable($this->tableNameFromObject($className));

        $this->query->setColumns($columns);

        $this->query->setWhereClause([
            [
                'field' => 'id',
                'operator' => '=',
                'value' => $objectId
            ]
        ]);

        $this->query->setPreparedValues($values);

        $sth = $this->pdo->prepare($this->mySQLQueryBuilder->buildUpdateObject($this->query));

        try {
            $sth->execute(!empty($this->query->getPreparedValues())
                ? $this->query->getPreparedValues()
                : null
            );
        } catch (PDOException $e) {
            throw new PDOException('Erreur PDO : ' . $e->getMessage());
        }
    }

    /**
     * Will transform object to INSERT INTO SQL query based on attributes
     * Returns id of inserted object
     *
     * @param $object
     * @return string
     */
    public function create($object)
    {
        $this->query->setTable($this->tableNameFromObject(get_class($object)));

        $sth = $this->pdo->prepare($this->mySQLQueryBuilder->buildCreate($this->query, $object));


        try {
            $sth->execute($this->query->getPreparedValues());
        } catch (PDOException $e) {
            throw new PDOException('Erreur PDO : ' . $e->getMessage());
        }

        return $this->pdo->lastInsertId();
    }

    /**
     * Deletes given object
     *
     * @param $object
     * @return bool
     */
    public function delete($object)
    {
        $this->query->setTable($this->tableNameFromObject(get_class($object)));

        $this->query->addPreparedValue($object->getId());

        $sth = $this->pdo->prepare($this->mySQLQueryBuilder->buildDelete($this->query));

        try {
            $sth->execute($this->query->getPreparedValues());
        } catch (PDOException $e) {
            throw new PDOException('Erreur PDO : ' . $e->getMessage());
        }

        return true;
    }

    /**
     * Deletes array of given objects from same entity
     *
     * @param $arrayOfObject
     * @return bool
     */
    public function deleteAll($arrayOfObject)
    {
        $this->query->setTable($this->tableNameFromObject(get_class($arrayOfObject[0])));

        $sth = $this->pdo->prepare($this->mySQLQueryBuilder->buildDeleteAll($this->query, $arrayOfObject));

        try {
            return $sth->execute($this->query->getPreparedValues());
        } catch (PDOException $e) {
            throw new PDOException('Erreur PDO : ' . $e->getMessage());
        }


    }

    /**
     * Will transform a fully qualified class name to its database equivalent
     * ie: App\Entity\MyCoolTable -> my_cool_table
     *
     * @param $className
     * @return string
     */
    private function tableNameFromObject($className)
    {
            $classExplode = explode('\\', $className);
            return StringUtilities::toSnakeCase(end($classExplode));
    }

    /**
     * Hydrates an entity from its array equivalent
     *
     * @param array $arrayResults
     * @param $className
     * @return array
     */
    private function hydrateArray(array $arrayResults, $className)
    {
        $arrayObjects = [];

        //Instantiation and hydratation of every returned results
        foreach ($arrayResults as $item) {

            $objectResult = new $className();
            $objectResult->hydrate($item);
            $arrayObjects[] = $objectResult;
        }

        return $arrayObjects;
    }
}
<?php

namespace App\Services\Utilities;

/**
 * Class StringUtilities
 *
 * Perform string manipulation
 *
 * @package App\Services\Utilities
 */
class StringUtilities
{
    /**
     * Removes capital letters and separate words with underscores
     *
     * ex: MyCoolClassName => my_cool_class_name
     *
     * @param $input
     * @return string
     */
    public static function toSnakeCase($input) {
        preg_match_all('!([A-Z][A-Z0-9]*(?=$|[A-Z][a-z0-9])|[A-Za-z][a-z0-9]+)!', $input, $matches);
        $ret = $matches[0];
        foreach ($ret as &$match) {
            $match = $match == strtoupper($match) ? strtolower($match) : lcfirst($match);
        }

        return implode('_', $ret);
    }

    /**
     * Capitalize words and removes underscores
     *
     * ex: my_cool_class_name => MyCoolClassName
     *
     * @param $snakeString
     * @return string
     */
    public static function snakeToPascalCase($snakeString)
    {
        $arr = explode('_', $snakeString);
        return implode('', array_map('ucfirst', $arr));
    }

    /**
     * Return ClassName from a fully qualified ClassName
     *
     * ex: App\Domain\Post\PostController => PostController
     *
     * @param $className
     * @return mixed
     */
    public static function trimNamespace($className) {
        $arrClassName = explode('\\', $className);
        return end($arrClassName);
    }

    /**
     * Useful for tokens
     *
     * @param int $length
     * @return string
     */
    public static function generateRandomString($length = 10) {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }
}
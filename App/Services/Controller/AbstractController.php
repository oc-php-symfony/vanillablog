<?php

namespace App\Services\Controller;

use App\Core\Application;
use App\Domain\Utility\Controller\UtilityController;
use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Error\SyntaxError;
use Twig\Loader\FilesystemLoader;

/**
 * Class AbstractController
 * Provide standard features for controllers
 *
 * @package App\Services\Controller
 */
abstract class AbstractController {

    /**
     * Allows the controller to render an twig processed html view
     *
     * @param $path
     * @param $args
     * @return string
     * @throws LoaderError
     * @throws RuntimeError
     * @throws SyntaxError
     */
    public function render($path, $args = [])
    {
        $loader = new FilesystemLoader(__DIR__.'/../../../templates');
        $twig = new Environment($loader, [
        'debug' => true
        ]);

        $twig->addExtension(new \Twig\Extension\DebugExtension());

        $twig->addGlobal('session', Application::$request->session());

        return $twig->render($path, $args);
    }

    /**
     * @param $location
     */
    protected function redirect($location)
    {
        Application::$request->setSession('redirect', true);
        header("location: $location");
        exit;
    }
}
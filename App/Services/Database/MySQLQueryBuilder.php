<?php

namespace App\Services\Database;

use App\Services\Utilities\StringUtilities;

/**
 * Class MySQLQueryBuilder
 *
 * returns string query from clauses
 *
 * @package App\Services\Database
 */
class MySQLQueryBuilder
{
    /**
     * Returns query string from Query object for a findAll request
     *
     * @param MySQLQuery $query
     * @return string
     */
    public function buildFindAll(MySQLQuery &$query)
    {
        //Base
        $queryBuild = "SELECT {$query->getFields()} FROM `{$query->getTable()}`";

        //Joins will only fetch references, not collections
        if ($query->getJoins() !== null) {
            foreach ($query->getJoins() as $classToJoin => $joinKey) {
                $joinTable = self::tableNameFromObject($classToJoin);
                $queryBuild .= " LEFT JOIN $joinTable ON {$query->getTable()}.$joinKey = $joinTable.id";
            }
        }

        //Where
        $queryBuild .= self::buildWhereClause($query);

        //Order
        $queryBuild .= $query->getOrderClause() ?: '';

        //Limit
        $queryBuild .= $query->getLimitClause() ?: '';

        return $queryBuild;
    }

    /**
     * Returns query string from Query object for a findFirst request
     *
     * @param MySQLQuery $query
     * @return string
     */
    public function buildFindFirst(MySQLQuery $query)
    {
        //Base
        $queryBuild = "SELECT {$query->getFields()} FROM `{$query->getTable()}`";

        //Joins will only fetch references, not collections
        if ($query->getJoins() !== null) {
            foreach ($query->getJoins() as $classToJoin => $joinKey) {
                $joinTable = self::tableNameFromObject($classToJoin);
                $queryBuild .= " LEFT JOIN $joinTable ON {$query->getTable()}.$joinKey = $joinTable.id";
            }
        }

        //Where
        $queryBuild .= self::buildWhereClause($query);

        return $queryBuild . " LIMIT 1";
    }

    /**
     * Returns query string from Query object for an update request
     *
     * @param MySQLQuery $query
     * @return string
     */
    public function buildUpdateObject(MySQLQuery $query)
    {
        $queryBuild = "UPDATE `{$query->getTable()}` SET ";

        foreach ($query->getColumns() as $column) {
            $queryBuild .= "$column = :$column, ";
        }

        $queryBuild = rtrim($queryBuild,', ');

        $queryBuild .= self::buildWhereClause($query);

        return $queryBuild;
    }

    /**
     * Returns query string from Query object for a create request
     *
     * @param MySQLQuery $query
     * @param Object $object
     * @return string
     */
    public function buildCreate(MySQLQuery &$query, Object $object)
    {
        //Base
        $queryBuild = "INSERT INTO `{$query->getTable()}` (";

        $methods = $this->methodsAndGetterInArray(get_class($object));

        foreach ($methods as $getter => $column) {
            $snakeColumn = StringUtilities::toSnakeCase($column);
            if ($getter !== 'getId' && !in_array($snakeColumn, get_class($object)::$noPersist)) {
                $query->addPreparedValue($object->$getter());
                $queryBuild .= "$snakeColumn, ";
            }
        }

        $queryBuild = rtrim($queryBuild,', ');
        $queryBuild .= ") VALUES (";

        for ($i = 0; $i < count($query->getPreparedValues()); $i++) {
            $queryBuild .= "?, ";
        }

        $queryBuild = rtrim($queryBuild,', ');
        $queryBuild .= ")";

        return $queryBuild;
    }

    /**
     * Returns query string from Query object for a delete request
     *
     * @param MySQLQuery $query
     * @return string
     */
    public function buildDelete(MySQLQuery $query)
    {
        //Base
        return "DELETE FROM `{$query->getTable()}` WHERE id = ?";
    }

    public function buildDeleteAll(MySQLQuery &$query, $arrayOfObject)
    {
        $queryBuild = "DELETE FROM `{$query->getTable()}` WHERE id";

        $inPlaceholder = "(";

        foreach ($arrayOfObject as $object) {
            $query->addPreparedValue($object->getId());
            $inPlaceholder .= "?, ";
        }

        $inPlaceholder = rtrim($inPlaceholder,', ');
        $inPlaceholder .= ")";

        $queryBuild .= " IN $inPlaceholder";

        return $queryBuild;
    }

    /**
     * Will transform a fully qualified class name to its database equivalent
     * ie: App\Entity\MyCoolTable -> my_cool_table
     *
     * @param $className
     * @return string
     */
    private static function tableNameFromObject($className)
    {
        $classExplode = explode('\\', $className);
        return StringUtilities::toSnakeCase(end($classExplode));
    }

    /**
     * Returns MySQL string of a where clause from an array of criteria
     *
     * @param MySQLQuery $query
     * @return string
     */
    private static function buildWhereClause(MySQLQuery &$query)
    {
        //Where clause only effective on the request main table
        if (!empty($query->getWhereClause())) {
            $whereClause = '';

            foreach ($query->getWhereClause() as $key => $whereCondition) {
                $whereField = $whereCondition['field'];
                $whereOperator = $whereCondition['operator'];

                if ($whereOperator === '=') {
                    if ($key === 0) {
                        $whereClause .= " WHERE $whereField = :{$key}whereValue";
                    } else {
                        $whereClause .= " AND $whereField = :{$key}whereValue";
                    }
                }

                $query->addPreparedValue($whereCondition['value'], ":{$key}whereValue");
            }
            return $whereClause;
        }
        return '';
    }

    /**
     * Will provide an array of database column from the object getters
     *
     * @param $className
     * @return array
     */
    private static function methodsAndGetterInArray($className)
    {
        $objectMethods = get_class_methods($className);
        $methods = [];

        foreach ($objectMethods as $objectMethod) {
            if (strpos($objectMethod, 'get') !== false) {
                $methods[$objectMethod] = lcfirst(ltrim($objectMethod, 'get'));
            }
        }

        return $methods;
    }
}
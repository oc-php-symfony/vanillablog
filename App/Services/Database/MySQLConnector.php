<?php

namespace App\Services\Database;

use App\Core\Application;
use PDO;
use PDOException;

/**
 * Class MySQLConnector
 *
 * Provides MySQL PDO connection
 *
 * @package App\Services\Database
 */
class MySQLConnector
{
    /**
     * Used for Singleton pattern check
     *
     * @var PDO
     */
    private static $pdo = null;

    /**
     * Will return existing PDO connection or create one if needed
     *
     * @return PDO
     */
    public static function getInstance()
    {
        if (self::$pdo === null) {
            try {
                $server = Application::$request->env('MYSQL_SERVER');
                $db = Application::$request->env('MYSQL_DATABASE');
                $port = Application::$request->env('MYSQL_PORT');
                $user = Application::$request->env('MYSQL_USER');
                $pwd = Application::$request->env('MYSQL_PWD');
                self::$pdo = new PDO("mysql:host={$server};dbname={$db};charset=utf8;port={$port};", $user, $pwd);
                self::$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
                self::$pdo->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);
            } catch (PDOException $e) {
                Throw new PDOException('Connexion échouée : ' . $e->getMessage());
            }
        }
        return self::$pdo;

    }
}

<?php

namespace App\Services\Database;

/**
 * Holds query clauses
 * Class MySQLQuery
 * @package App\Services\Database
 */
class MySQLQuery
{
    private $table;
    private $fields;
    private $joins;
    private $columns;
    private $whereClause;
    private $orderClause;
    private $limitClause;
    private $preparedValues;

    /**
     * @return mixed
     */
    public function getTable()
    {
        return $this->table;
    }

    /**
     * @param mixed $table
     */
    public function setTable($table): void
    {
        $this->table = $table;
    }

    /**
     * @return mixed
     */
    public function getFields()
    {
        return $this->fields;
    }

    /**
     * @param mixed $fields
     */
    public function setFields($fields): void
    {
        $this->fields = $fields;
    }

    /**
     * @return mixed
     */
    public function getJoins()
    {
        return $this->joins;
    }

    /**
     * @param mixed $joins
     */
    public function setJoins($joins): void
    {
        $this->joins = $joins;
    }

    /**
     * @return mixed
     */
    public function getColumns()
    {
        return $this->columns;
    }

    /**
     * @param mixed $columns
     */
    public function setColumns($columns): void
    {
        $this->columns = $columns;
    }

    /**
     * @return mixed
     */
    public function getWhereClause()
    {
        return $this->whereClause;
    }

    /**
     * @param mixed $whereClause
     */
    public function setWhereClause($whereClause): void
    {
        $this->whereClause = $whereClause;
    }

    /**
     * @return mixed
     */
    public function getOrderClause()
    {
        return $this->orderClause;
    }

    /**
     * @param mixed $orderClause
     */
    public function setOrderClause($orderClause): void
    {
        $this->orderClause = $orderClause;
    }

    /**
     * @return mixed
     */
    public function getLimitClause()
    {
        return $this->limitClause;
    }

    /**
     * @param mixed $limitClause
     */
    public function setLimitClause($limitClause): void
    {
        $this->limitClause = $limitClause;
    }

    /**
     * @return mixed
     */
    public function getPreparedValues()
    {
        return $this->preparedValues;
    }

    public function addPreparedValue($preparedValue, $key = null)
    {
        if ($key !== null) {
            $this->preparedValues[$key] = $preparedValue;
        } else {
            $this->preparedValues[] = $preparedValue;
        }
    }

    /**
     * @param mixed $preparedValues
     */
    public function setPreparedValues($preparedValues): void
    {
        $this->preparedValues = $preparedValues;
    }

}
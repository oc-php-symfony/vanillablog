Vanilla Blog
============

# Prerequisites

Install Docker environment @ https://gitlab.com/DamienAdam/dockervanillablog.git

# Launch

Launch docker environment and go to 127.0.0.1
PhpMyadmin available at 127.0.0.1:8080

# Folder structure

## vendor
Composer's 3rdparty libraries live here

## templates
View part of the application, essentially twig templates, sorted in domains

## public
Public web folder, any frontend matters lives here

## dumps
Will contain dumps of the database

## App

### Services 
Services are essentially Abstract classes to be extended in domains or for kernel use, these are used for :
Routing, Database abstraction, Form building and validation, Utilities...

### Domain
Domain sorted elements of the application, for this blog they are :
Post, User, Comment.
